package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"sensor-listener/models"
	"sensor-listener/repositories"
	"sensor-listener/utils"
	"time"
)

// Controllers
func GetMessages(w http.ResponseWriter, _ *http.Request) {
	// fetch all
	messages := repositories.FindAll()
	// marshalling
	bytes, err := json.Marshal(messages)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	// write out response
	utils.WriteJsonResponse(w, bytes)
}

func SaveMessage(w http.ResponseWriter, r *http.Request) {
	// parse body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	// create a new notification
	msg := new(models.Message)
	err = json.Unmarshal(body, msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	msg.Created = time.Now()
	// save it
	repositories.Save(msg)
	w.WriteHeader(http.StatusCreated)
}

