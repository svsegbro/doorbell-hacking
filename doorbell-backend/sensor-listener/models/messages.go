package models

import "time"

type Message struct {
	Msg string `json: msg`
	Created time.Time `json: created`
}
