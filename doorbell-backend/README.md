# Doorbell backend
We fire up the doorbell backend system using Docker-Compose. Following containers are used:
* Sensor listener.
  * Exposes a RESTful API, to which sensors can post events (e.g. heartbeat signals, button pressed, etc). 
  * A doorbell is considered as a kind of sensor.
* Message broker.
  * A RabbitMQ container.
  * Different topics, according to message type. 
* Notification engine.
  * Subscribes to specific topics.
  * Executes corresponding actions.

