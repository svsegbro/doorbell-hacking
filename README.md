# Upcyling your doorbell 

Starting configuration is a regular doorbell, consisting of a momentary pushbutton connected directly to a gong. 

Two major drawbacks with this setup:
* The gong inside did not always sound loud and clear, depending on how you push the button outside.
* No additional notifications when you are away from the gong (e.g. in the garden, in the garage).

Let's re-use the material we have and create something better out of it. 

## Project structure
This repository consists of the following main parts:
* Folder *doorbell-initial*:
  * Details on initial configuration.
* Folder *doorbell-upcycled*:
  * Details on the upcycled doorbell device, reusing existing pushbutton and gong. 
  * We put a microcontroller in between button and gong, together with some additional components to make it work properly.
* Folder *doorbell-backend*:
  * Details on the backend system which handles incoming doorbell events. 

