int pinButton = 5; 
int pinRelais = 4;  
int switchState = 0;

void setup()
{
  pinMode(pinButton,INPUT);
  pinMode(pinRelais,OUTPUT);
}

void loop()
{
  // read button
  switchState = digitalRead(pinButton);
  
  // update relais mode
  if(switchState == HIGH)
  {
    digitalWrite(pinRelais,LOW);
  } 
  // button is pressed
  else
  {
    digitalWrite(pinRelais,HIGH);
    delay(250);
    digitalWrite(pinRelais,LOW);
    delay(250);
    digitalWrite(pinRelais,HIGH);
    delay(250);
  }
}
